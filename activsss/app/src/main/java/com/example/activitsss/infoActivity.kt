package com.example.activitsss

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val age = intent.getIntExtra("Age", 0)
        findViewById<TextView>(R.id.AgeText).text = "Your age is $age"
        val mText = findViewById<TextView>(R.id.MainText)
        when(age)
        {
            in 1..10 -> mText.setText(R.string.child)
            in 11..25 -> mText.setText(R.string.young)
            in 26..49 -> mText.setText(R.string.adult)
            in 50..99 -> mText.setText(R.string.old)
            else -> mText.setText(R.string.trueOld)

        }
    }
}
