package com.example.projectv

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val image: ImageView = findViewById(R.id.Image)
        val button: Button = findViewById(R.id.Button)
        button.setOnClickListener {
            when(Random.nextInt(1,7)){
                1 -> image.setImageDrawable(getDrawable(R.drawable.aaaa))
                2 -> image.setImageDrawable(getDrawable(R.drawable.bbbb))
                3 -> image.setImageDrawable(getDrawable(R.drawable.ddd))
                4 -> image.setImageDrawable(getDrawable(R.drawable.kva))
                5 -> image.setImageDrawable(getDrawable(R.drawable.mem))
                6 -> image.setImageDrawable(getDrawable(R.drawable.pavyk))
            }
            val color = Color.argb(255,
                Random.nextInt(0,255),
                Random.nextInt(0,255),
                Random.nextInt(0,255))
            button.setBackgroundColor(color)
        }
    }
}
